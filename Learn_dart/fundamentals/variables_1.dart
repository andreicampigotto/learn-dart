import 'dart:io'; //importa io

main(){
  
  int a = 7; //igual c# e java
  double b = 7.1;  //igual c# e java
  var bla = 17.7; //funciona tambem igual c# por inferencia
  var val= "the bla value is:"; // var e valor de inferencia;
  
  var inputs;
  stdout.write("please enter input value:"); //comando de saida semelhante ao print
  inputs = stdin.readLineSync(); //comando para receber leitura de entrada

  print(a + b);
  print(val + (bla).toString()); //converte igual c#
}