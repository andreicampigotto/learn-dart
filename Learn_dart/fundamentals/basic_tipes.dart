/*
  List
  Set
  Map
*/
 main() {

   //aceita repeticao..
   List approved = ['Ana', 'Maria', 'Carlos'];

    print(approved is List);
    print(approved);
    print(approved.elementAt(2));
    print(approved[0]);
    print(approved.length);

  //nao aceita repeticao
  Map phones ={

    //key  : value

    'Jhon' : '+55 (47) 96989 - 0032',
    'Andrei' : '+55 (47) 99670-7117',
    'Camila' : '+1 (240) 785 - 673',
  };

    print(phones is Map);
    print(phones);
    print(phones['Camila']);
    print(phones.length);
    print(phones.values);
    print(phones.keys);
    print(phones.entries);

  //nao aceita repeticao
  var teams = { 'Dallas', 'Giants', 'Miami', 'Bills' 'Patriots'};

    print(teams is Set);
    teams.add('Jets');
    print(teams.length);
    print(teams.first);
    print(teams.last);
 }
