main() {
  String name = "Jhon";
  String stats = "Aproved";
  double note = 7.1;
 
  //Sem interpolacao 
  String phrase1 = name + " is " + stats + " because note: " + note.toString() + ("! ");
  print(phrase1);

  String phrase2 = "$name is $stats because note: $note! ";
  print(phrase2);
}