main(){
  salutePerson(name: "Jhon", age: 21);
  salutePerson(age: 22, name: "Catharina");
  
  printDate();
  printDate(year: 1995);
}
salutePerson({String name, int age}){
  print("Hello $name really you have only $age?");
}
printDate({int moth = 07, int day = 17, int year = 1771}){
  print("$day/$moth/$year");
}