main(){
  //normal
  //type name = value;
  //ex:
  int x = 2;

   //type name = value;
   //dont work
  /*int Function(int int) sum1 = sumFN;
  print(sum1(10, 7));
  */

  /*
 int Function(int int) sum2 = (x , y){
   return x + y;
 };
  print(sum2(10,7));
 */

  var sum3 = ([int x, int y]){
    return x + y;
  };

  print(sum3(10,7));
}

int sumFN(int a, int b){
  return a + b ;
}