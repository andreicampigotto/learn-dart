main(){

  Map<String, double> notes = {
    'John Injection' : 7.1,
    'Ed Simas'       : 1.7,
    'Amanda Maria'   : 5.1, 
  };

  for (String name in notes.keys){
    print("The student name is: $name note: ${notes[name]}.");
  }

 for (var register in notes.entries){
    print("The: ${register.key} : ${register.value}.");
  } 

}